// Based on the Paho C code example from www.eclipse.org/paho/
#include <iostream>
#include <sstream>
#include <fstream>
#include <string.h>
#include "MQTTClient.h"
#include <stdio.h>
#include <time.h>
#define  CPU_TEMP "/sys/class/thermal/thermal_zone0/temp"
using namespace std;

//Please replace the following address with the address of your server
#define ADDRESS    "tcp://192.168.1.12:1883"
#define CLIENTID   "rpil"
#define AUTHMETHOD "del"
#define AUTHTOKEN  "climbing"
#define TOPIC      "ee513/CPUTemp"
#define QOS        1
#define TIMEOUT    10000L

const char* getTime ()
{
  time_t rawtime;
  struct tm * timeinfo;
  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  printf ( "Current local time and date: %s", asctime (timeinfo) );
  return asctime (timeinfo);
}


//std::string cpuLoad()
//long long unsigned int cpuLoad()
long long unsigned cpuLoad()
{
    FILE* file;
    long long unsigned totalUser;

    file = fopen("/proc/stat", "r");
    fscanf(file, "cpu %llu", &totalUser);
    fclose(file);
   
    printf ("cpu load %llu \n",totalUser); 
    return totalUser;
}



float getCPUTemperature() {        // get the CPU temperature
   int cpuTemp;                    // store as an int
   fstream fs;
   fs.open(CPU_TEMP, fstream::in); // read from the file
   fs >> cpuTemp;
   fs.close();
   return (((float)cpuTemp)/1000);
}



int main(int argc, char* argv[]) {
   //cpuLoad();
   char str_payload[100];          // Set your max message size here
   MQTTClient client;
   MQTTClient_connectOptions opts = MQTTClient_connectOptions_initializer;
   MQTTClient_willOptions wopts = MQTTClient_willOptions_initializer; // last will message!
   MQTTClient_message pubmsg = MQTTClient_message_initializer;
   MQTTClient_deliveryToken token;
   MQTTClient_create(&client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
   opts.keepAliveInterval = 20;
   opts.cleansession = 1;
   opts.username = AUTHMETHOD;
   opts.password = AUTHTOKEN;
   
   wopts.topicName = TOPIC;
   wopts.message = "RPI not connecting";
   wopts.qos = 1; //everyone gets it once if set to 1

   int rc;
   if ((rc = MQTTClient_connect(client, &opts)) != MQTTCLIENT_SUCCESS) {
      cout << "Failed to connect, return code " << rc << endl;
      return -1;
   }
   sprintf(str_payload, "{\"d\":{\"CPUTemp\": %f,\"CPU Load\":%llu,\"Time\": %s }}", getCPUTemperature(),cpuLoad(),getTime());
   //sprintf(str_payload, "{\"d\":{\"CPUTemp\": %f,\"Time\": %s }}", getCPUTemperature(),getTime());
   pubmsg.payload = str_payload;
   pubmsg.payloadlen = strlen(str_payload);
   pubmsg.qos = QOS;
   pubmsg.retained = 0;
   MQTTClient_publishMessage(client, TOPIC, &pubmsg, &token);
   cout << "Waiting for up to " << (int)(TIMEOUT/1000) <<
        " seconds for publication of " << str_payload <<
        " \non topic " << TOPIC << " for ClientID: " << CLIENTID << endl;
   rc = MQTTClient_waitForCompletion(client, token, TIMEOUT);
   cout << "Message with token " << (int)token << " delivered." << endl;
   //MQTTClient_disconnect(client, 10000);
   MQTTClient_destroy(&client);
   return rc;
}
