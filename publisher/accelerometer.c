
/*
 * 
 *  Created on: 18-04-2021
 *      Author: D. Aherne
 *      (modified code from Dr. Derek Molloy)
 */

#include<stdio.h>
#include<fcntl.h>
#include<sys/ioctl.h>
#include<unistd.h>
#include<linux/i2c.h>
#include<linux/i2c-dev.h>
#include <stdint.h>
#include <string.h>
#define DATAX0 0x32
#define DATAX1 0x33
#define DATAY0 0x34
#define DATAY1 0x35
#define DATAZ0 0x36
#define DATAZ1 0x37
#define POWER_CTL 0x2D
#define FIFO_MODE 0x38

// Convert binary coded decimal to readable decimal numbers
int bcdToDec(uint8_t b)
{
    return ((b / 16) * 10 + (b % 16));
}

// and vice versa
uint8_t decToBcd(int d)
{
    return(decToBcd)((d / 10 * 16) + (d % 10));
}

// read data 

uint8_t readByte(uint8_t* address)
{

   int file;
   uint8_t temp[1];

    if ((file = open("/dev/i2c-1", O_RDWR)) < 0) {
        perror("failed to open the bus\n");
        return -1;
    }

    if (ioctl(file, I2C_SLAVE, 0x53) < 0) {
        perror("Failed to connect\n");
        return -1;
    }

    // first write to get specified register
    if (write(file, address, 1) != 1) {
        perror("Failed to write\n");
        return -1;
    }

    // now read from specified register
    if (read(file, temp, 1) != 1) {
        perror("Failed to read\n");
        return 1;
    }

    close(file);
    return temp[0];
}

// write data 
bool writeByte(uint8_t* addressAndValue)
{
    int file;

    if ((file = open("/dev/i2c-1", O_RDWR)) < 0) {
        perror("failed to open the bus\n");
        return false;
    }

    if (ioctl(file, I2C_SLAVE, 0x53) < 0) {
        perror("Failed to connect \n");
        return false;
    }

    // write value to the specified register
    if (write(file, addressAndValue, 2) != 2) {
        perror("Failed to reset the register data\n");
        return false;
    }

    close(file);
    return true;
}

// enable Accelerometer
bool enableAccelerometer()
{
    uint8_t writeBuf[2];
    writeBuf[0] = FIFO_MODE;
    writeBuf[1] = 0x80;
    if (writeByte(writeBuf) != true) {
        return false;
    }
    writeBuf[0] = POWER_CTL;
    writeBuf[1] = 0x80; // enable measurements
    if (writeByte(writeBuf) != true) {
        return false;
    }
    return true;
}

// disable
bool disableAccelerometer()
{
    uint8_t writeBuf[2];
    writeBuf[0] = FIFO_MODE;
    writeBuf[1] = 0;
    if (writeByte(writeBuf) != true) {
        return false;
    }
    return true;
}

// read Accelerometer Data
int xAccData()
{
    uint8_t address[1];
    // get X
    address[0] = DATAX0;
    int x0 = readByte(address);
    if (x0 < 0) {
        perror("Failed to read X data0 from Accelerometer\n");
    }
    address[0] = DATAX1;
    int x1 = readByte(address);
    if (x1 < 0) {
        perror("Failed to read X data1 from Accelerometer\n");
    }
    return bcdToDec((x1<<8)|(x0));
}
 
int yAccData()
{   
    uint8_t address[1];
    // get Y
    address[0] = DATAY0;
    int y0 = readByte(address);
    if (y0 < 0) {
        perror("Failed to read Y data0 from Accelerometer\n");
    }
    address[0] = DATAY1;
    int y1 = readByte(address);
    if (y1 < 0) {
        perror("Failed to read Y data1 from Accelerometer\n");
    }
    return bcdToDec((y1<<8)|(y0));
}

int zAccData()
{
    uint8_t address[1];
    // get Z
    address[0] = DATAZ0;
    int z0 = readByte(address);
    if (z0 < 0) {
        perror("Failed to read Z data0 from Accelerometer\n");
    }
    address[0] = DATAZ1;
    int z1 = readByte(address);
    if (z1 < 0) {
        perror("Failed to read Z data1 from Accelerometer\n");
    }
    return bcdToDec((z1<<8)|(z0));
}

