EE513: Assignment 2 2020/2021

Course: GCIOT1

Module : EE513 Connected Embedded Systems

date: 11/04/2021
student: Derek Aherne
student num: 19215728

Aims and Objectives
Internet Connected Embedded: In this assignment students must develop a full-stack IoT solution that interfaces to real-world sensors. This assignment is worth 15% of the overall module mark.
