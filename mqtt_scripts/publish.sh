#! /usr/bin/env bash
echo "Installed version of mosquitto: "
mosquitto -h
echo "checking port 1883:"
netstat -at | grep 1883
echo "displaying mosquitto files:"
ls -l /etc/mosquitto/
echo "enable password protection, by adding the below lines to the config file"
echo "password_file /etc/mosquitto/passwd" >> /etc/mosquitto/mosquitto.conf
echo "allow_anonymous false" >> /etc/mosquitto/mosquitto.conf
tail -n 2 /etc/mosquitto/mosquitto.conf
echo "this allows a publisher to create a password using sudo mosquitto_passwd -c del climbing"
mosquitto_passwd -b /etc/mosquitto/passwd del climbing
echo "contents of password file"
more /etc/mosquitto/passwd
echo "restart the mosquitto server"
systemctl restart mosquitto
echo "attempt to publish using a password (in debug mode):"
mosquitto_pub -d -t 'ee513/test' -m "Hello World" -P  climbing -u del
